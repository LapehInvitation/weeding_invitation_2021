<!-- Photo Gallery -->

<!-- Section: Gallery -->
<section id="pixiefy-wedz-photo-gallery" class="section-padding wedz-section">
    <div class="inner-wedz-photo-gallery">
        <div class="wedz-section-header">
            <img src="<?= base_url() ?>assets/images/phone-gallery-section-icon.png" alt="Section Header Icon" class="img-responsive">
            <h2>Photo Gallery</h2>
        </div> <!-- End Wedz Section Header -->

        <!--/section-heading -->
        <div class="container-fluid">
            <!-- row fluid -->
            <div class="row-fluid">
                <!-- Navigation -->
                <div class="text-center col-md-12">
                    <div class="text-center col-md-4">
                        <ul class="nav nav-pills category text-center" role="tablist" id="gallerytab">
                            <li class="active"><a href="#" data-toggle="tab" data-filter="*">All</a>
                        </ul>
                    </div>
                    <div class="text-center col-md-4">
                        <ul class="nav nav-pills category text-center" role="tablist" id="gallerytab">
                            <li><a href="#" data-toggle="tab" data-filter=".our-photos">Our Photos</a></li>
                        </ul>
                    </div>
                    <div class="text-center col-md-4">
                        <ul class="nav nav-pills category text-center" role="tablist" id="gallerytab">
                            <li><a href="#" data-toggle="tab" data-filter=".wedding">Wedding</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Gallery -->
                <div class="col-md-12 gallery margin1">
                    <div id="lightbox">
                        <!-- Image 1 -->
                        <div class="wedding col-lg-4 col-sm-6 col-md-6">
                            <div class="isotope-item">
                                <div class="gallery-thumb">
                                    <a data-lightbox-gallery="gallery1" class="photostack-img" href="<?= base_url() ?>assets/images/gallery7.jpg" title="You can add caption to pictures.">
                                        <span class="overlay-mask"></span>
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/gallery1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 2 -->
                        <div class="our-photos col-lg-4 col-sm-6 col-md-6">
                            <div class="isotope-item">
                                <div class="gallery-thumb">
                                    <a data-lightbox-gallery="gallery1" class="photostack-img" href="<?= base_url() ?>assets/images/gallery7.jpg" title="You can add caption to pictures.">
                                        <span class="overlay-mask"></span>
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/gallery2.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 3 -->
                        <div class="wedding col-lg-4 col-sm-6 col-md-6">
                            <div class="isotope-item">
                                <div class="gallery-thumb">
                                    <a data-lightbox-gallery="gallery1" class="photostack-img" href="<?= base_url() ?>assets/images/gallery7.jpg" title="You can add caption to pictures.">
                                        <span class="overlay-mask"></span>
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/gallery3.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 4 -->
                        <div class="wedding col-lg-4 col-sm-6 col-md-6">
                            <div class="isotope-item">
                                <div class="gallery-thumb">
                                    <a data-lightbox-gallery="gallery1" class="photostack-img" href="<?= base_url() ?>assets/images/gallery7.jpg" title="You can add caption to pictures.">
                                        <span class="overlay-mask"></span>
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/gallery4.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 5 -->
                        <div class="wedding col-lg-4 col-sm-6 col-md-6">
                            <div class="isotope-item">
                                <div class="gallery-thumb">
                                    <a data-lightbox-gallery="gallery1" class="photostack-img" href="<?= base_url() ?>assets/images/gallery7.jpg" title="You can add caption to pictures.">
                                        <span class="overlay-mask"></span>
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/gallery5.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 6 -->
                        <div class="our-photos col-lg-4 col-sm-6 col-md-6">
                            <div class="isotope-item">
                                <div class="gallery-thumb">
                                    <a data-lightbox-gallery="gallery1" class="photostack-img" href="<?= base_url() ?>assets/images/gallery7.jpg" title="You can add caption to pictures.">
                                        <span class="overlay-mask"></span>
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/gallery6.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- Image 7 -->
                        <div class="our-photos col-lg-4 col-sm-6 col-md-6">
                            <div class="isotope-item">
                                <div class="gallery-thumb">
                                    <a data-lightbox-gallery="gallery1" class="photostack-img" href="<?= base_url() ?>assets/images/gallery7.jpg" title="You can add caption to pictures.">
                                        <span class="overlay-mask"></span>
                                        <img class="img-responsive" src="<?= base_url() ?>assets/images/gallery7.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /lightbox-->
                </div>
                <!-- /col-lg-12 -->
            </div>
            <!-- /row fluid -->
        </div>
        <!-- /container fluid-->
    </div>


</section>
<!-- /Section ends -->