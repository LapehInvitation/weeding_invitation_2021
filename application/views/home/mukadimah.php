<!-- Our Story -->
<section id="pixiefy-wedz-our-story" class="section-padding wedz-section yellow_section section_gap">
	<div class="container">
		<div class="inner-wedz-our-story">

			<div class="wedz-section-header fadein">
				<img src="<?= base_url() ?>assets/images/bismillah.png" alt="Section Header Icon" class="img-responsive"
				style="max-width: 90%;">
				<p>
				    <strong>Assalamu’alaikum Warahmatullahi Wabarakaatuh</strong>
					<br/>
					Dengan mengharap Ridho Allah SWT dan Atas Karunianya mempertemukan kami dalam kasih pernikahan. 
					Dengan doa restu seluruh kerabat dan sahabat ijin kan kami menaikan jangkar bahtera pernikahan kami, 
					berlayar menuju samudera nan luas. 
				</p>
			</div> <!-- End Wedz Section Header -->

			<div class="row clearfix theCouple our-love-life-photo">
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 fadein" style="margin-bottom: 45px;">
					<img src="assets/images/groom.png" alt=" " class="bigRound" />
					<h3>Ridlo Putuismaya</h3>
					<h5>Putra dari</h5>
					<h5>Bapak Totok Mudjianto</h5>
					<h5>&</p>
					<h5>Ibu Daryati</h5>
					<ul>
					</ul>
				</div>
				<div class="col-md-2 col-lg-2 line fadein"> <i class="fa fa-heart"></i> </div>
				<div class="col-xs-12 col-sm-12 unline fadein" > <p>&</p> </div>
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 fadein">
					<img src="assets/images/bride.png" alt=" " class="bigRound" />
					<h3>Siti Nur Lusiawati</h3>
					<h5>Putri dari</h5>
					<h5>Bapak Dahli</h5>
					<h5>&</h5>
					<h5>Ibu Tri Supriyati</h5>
					<ul>
					</ul>
				</div>
			</div>

		</div> <!-- ./End Inner Our Story -->
	</div>
</section>
<!-- ./End Our Story -->