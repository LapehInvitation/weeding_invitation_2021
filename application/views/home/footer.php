<!-- Footer -->
<footer id="pixiefy-wedz-footer-section" class="wedz-section">
            <div class="container">
                <div class="inner-footer-section">

                    <div class="footer-main-content"></div>

                </div> <!-- ./End Inner Footer Section -->
            </div>
        </footer><!-- ./End Footer -->
        <div class="container">
          <div class="inner-wedz-gift-registration">
             <div class="gift-registration-main-content">
               <p style="font-size: 10px;">Lha Peh Creative Production | Website and Creative Digital Programming | Creative Digital Content Creator</p>
        </div></div></div>
        <!-- WEDZ JS FILES -->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.0.2.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/classie.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/photostack.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;AMP;"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/wow.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.superslides.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/smoothscroll.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/main.js"></script>
        
        <!-- galery -->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/galery/main.js"></script> 
        <script type="text/javascript" src="<?= base_url() ?>assets/js/galery/jquery.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/galery/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/galery/plugins.js"></script>

        
        <!-- ANIMATION -->
        <script>
        $(document).ready(function() {
            /* Every time the window is scrolled ... */
            $(window).scroll( function(){
                /* Check the location of each desired element */
                $('.fadein').each( function(i){
                    var bottom_of_object = $(this).position().top + $(this).outerHeight();
                    var bottom_of_window = $(window).scrollTop() + $(window).height();
                    /* If the object is completely visible in the window, fade it it */
                    if( bottom_of_window > bottom_of_object ){
                        $(this).animate({'opacity':'1'},500);
                    }
                }); 
            });
            
        });
        </script>

    </body>

<!-- Mirrored from pixiefy.com/themes/wedz/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 01 May 2021 06:04:27 GMT -->
</html>
