<!-- timer -->
<section id="pixiefy-wedz-counter" class="section-padding wedz-section">
            <div class="container">
                <div class="inner-wedz-counter">

                    <div class="row">
                        <div class="wedz-counter-main-content">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 fadein">
                                <div class="single-wedz-counter">
                                    <!-- <h6 class="numeric-count number-animate" data-value="165" data-animation-duration="1000">0</h6> -->
                                    <h6 class="numeric-count number-animate" id="hari"></h6>
                                    <span>Hari</span>
                                </div>
                            </div> <!-- ./End Single Wedz Counter -->

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 fadein">
                                <div class="single-wedz-counter">
                                    <h6 class="numeric-count number-animate" id="jam"></h6>
                                    <!-- <h6 class="numeric-count number-animate" data-value="15" data-animation-duration="1000">0</h6> -->
                                    <span>Jam</span>
                                </div>
                            </div> <!-- ./End Single Wedz Counter -->

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 fadein">
                                <div class="single-wedz-counter">
                                    <h6 class="numeric-count number-animate" id="menit"></h6>
                                    <!-- <h6 class="numeric-count number-animate" data-value="26" data-animation-duration="1000">0</h6> -->
                                    <span>Menit</span>
                                </div>
                            </div> <!-- ./End Single Wedz Counter -->

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 fadein">
                                <div class="single-wedz-counter">
                                    <h6 class="numeric-count number-animate" id="detik"></h6>
                                    <!-- <h6 class="numeric-count number-animate" data-value="43" data-animation-duration="1000">0</h6> -->
                                    <span>Detik</span>
                                </div>
                            </div> <!-- ./End Single Wedz Counter -->
                        </div> <!-- ./End Wedz Counter Main Content -->
                    </div>

                    <div class="counter-end-tagline fadein">
                        <h2>Menuju Hari Bahagia Kami</h2>
                    </div>

                </div> <!-- ./End Inner Wedz Counter -->
            </div>
            <div class="wedz-section-overley gold-overley"></div>
        </section><!-- ./End Wedz Counter -->

        <script>
            // Set the date we're counting down to
            var countDownDate = new Date("Jul 18, 2021 18:00:00").getTime();

            // Update the count down every 1 second
            var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("hari").innerHTML = days;
            document.getElementById("jam").innerHTML = hours;
            document.getElementById("menit").innerHTML = minutes;
            document.getElementById("detik").innerHTML = seconds;

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("hari").innerHTML = "0";
                document.getElementById("jam").innerHTML = "0";
                document.getElementById("menit").innerHTML = "0";
                document.getElementById("detik").innerHTML = "0";
            }
            }, 1000);
        </script>
        <!-- end timer -->

        <!-- Wedding Ceremony -->
        <section id="pixiefy-wedz-wedding-ceremony" class="wedz-section">
            <div class="container">
                <div class="inner-wedding-ceremony">

                    <div class="wedz-section-header fadein">
                        <img src="<?= base_url() ?>assets/images/wedding-ceremony-secon-icon.png" alt="Section Header Icon" class="img-responsive">
                        <h2>Akad Dan Resepsi Pernikahan</h2>
                    </div> <!-- End Wedz Section Header -->

                    <div class="wedz-tree">
                        <img src="<?= base_url() ?>assets/images/tree.png" alt="" class="responsive">
                    </div>

                    <div class="wedding-ceremony-main-content">

                        <div class="wedding-ceremony-header-content rolade fadein">
                            <header>
                                <span>Akad Ijab Kabul</span>
                                <h4>Ridlo Putu Ismaya <br> & <br> Siti Nur Lusiawati </h4>
                            </header>

                            <p>Mengundang anda pada Janji Suci Ijab Kabul dan Resepsi Pesta Pernikahan Kami</p>
                        </div> <!-- ./End Wedding Ceremony Header Content -->
                        <div class="wedding-ceremony-footer-content fadein">
                          <img src="<?= base_url() ?>assets/images/wedding-ceremony-secon-icon.png" alt="Section Header Icon" class="img-responsive">
                          <h3>18 JULI 2021</h3>
                        </div>
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 fadein" style="margin-bottom: 25px; margin-top: 25px">
                                <div class="signle-date-time-wed-party">
                                    <h3>Akad Nikah</h3>
                                    <h4>16.00 s/d 17.00 WIB</h4>
                                    <br><a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=N2c4bTZ1Z2sxcjNyOThwZ2NtamxhcWtvaWQgeXRzYXR1dTAyQG0&amp;tmsrc=ytsatuu02%40gmail.com"><button type="button">Simpan Di Kalender</button></a>
                                </div>
                            </div> <!-- ./End Single Date and Time Wedding Party -->

                            <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 fadein" style="margin-bottom: 25px; margin-top: 25px">
                                <div class="signle-date-time-wed-party">
                                    <h3>Resepsi</h3>
                                    <h4>18.00 s/d 20.00 WIB</h4>
                                    <br><a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=NGZuN2k1dWU0MDBkMDZya2hqdG12NjhidWwgeXRzYXR1dTAyQG0&amp;tmsrc=ytsatuu02%40gmail.com"><button type="button">Simpan Di Kalender</button></a>
                                </div>
                            </div> <!-- ./End Single Date and Time Wedding Party -->

                             <!-- ./End Single Date and Time Wedding Party -->

                        </div>

                        <div class="wedding-ceremony-footer-content fadein">
                            <p>Sungguh suatu kebahagiaan untuk hadir di</p>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 25px;">
                              <div class="signle-date-time-wed-party">
                                  <h3>Lokasi</h3>
                                  <h4>Gedung Grand Admiral, Semarang <br> Jl. Ki Mangunsarkoro No.38 Karangkidul, Semarang Tengah</h4>
                              </div>
                            </div>

                            <a href="https://maps.google.com/maps?ll=-6.992585,110.429153&z=16&t=m&hl=id&gl=ID&mapclient=embed&daddr=Grand%20Admiral%20Ballroom%20Jl.%20Ki%20Mangunsarkoro%20No.38%20Karangkidul%20Kec.%20Semarang%20Tengah%2C%20Kota%20Semarang%2C%20Jawa%20Tengah%2050136@-6.992585099999999,110.4291529" class="wedz-button2" data-lightbox-type="inline">Lihat Rute Lokasi</a>

                            <div id="wedm" style="display:none">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sbn!2sbd!4v1448516651702" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div> <!-- ./End Wedding Ceremony Footer Content -->

                    </div> <!-- ./End Wedding Ceremony Main Content -->


                </div> <!-- ./End Inner Wedding Ceremony -->
            </div>
        </section><!-- ./End Wedding Ceremony -->

        <!-- Wedding Party -->

        <section id="pixiefy-wedz-wedding-party" class="section-padding wedz-section">
            <div class="container">
                <div class="inner-wedz-wedding-party">
                    <!-- Resepsi Hilang -->
<!--                     <div class="wedz-section-header">
                        <img src="<?= base_url() ?>assets/images/wedding-ceremony-secon-icon.png" alt="Section Header Icon" class="img-responsive">
                        <h2>Resepsi Pernikahan</h2>
                    </div> <!-- End Wedz Section Header -->

<!--                     <div class="wedding-party-column">

                        <div class="top-content-wedding-party">
                            <div class="gallery-thumb-wed-party section-padding">
                                <ul>
                                    <li>
                                        <a href="images/wedding-party-1-big.jpg"  data-lightbox-gallery="gallery2" class="partystock-img">
                                            <img src="<?= base_url() ?>assets/images/wedding-party-1.jpg" alt="Wedding Party Fun" class="img-responsive">
                                            <div class="single-wedz-photo-overley"></div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="images/wedding-party-2-big.jpg"  data-lightbox-gallery="gallery2" class="partystock-img">
                                            <img src="<?= base_url() ?>assets/images/wedding-party-2.jpg" alt="Wedding Party Place" class="img-responsive">
                                            <div class="single-wedz-photo-overley"></div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="images/wedding-party-3-big.jpg"  data-lightbox-gallery="gallery2" class="partystock-img">
                                            <img src="<?= base_url() ?>assets/images/wedding-party-3.jpg" alt="Wedding Party Food" class="img-responsive">
                                            <div class="single-wedz-photo-overley"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div> <!-- ./End Gallery Thumb Wedding Party -->

<!--                             <p>Terkait dengan masa Pandemi Corona Virus 19 (Covid19), Pemerintah Republik Indonesia memperintahkan untuk melaksanakan protokol kesehatan dan aturan sesuai dengan anjuran Satgas Covid19 Nasional tentang pelaksanaan pengumpulan masa atau keramaian.</p>

                            <p>Maka Kegiatan Resepsi yang akan kami laksanakan akan dikondisikan sedemikian rupa dengan membatasi jumlah tamu yang akan hadir 50% dari kapasitas gedung dan dibagi dalam beberapa seksi. Kegiatan juga akan dilaksanakan dengan protokol kesehatan
                            Mencuci Tangan / Handsanitizer, Menjaga Jarak, dan Wajib mengenakan masker. </p>
                        </div>  ./end Top Content Wedding Party -->

 <!--                        <div class="date-time-wedding-party section-padding">
                            <div class="row">

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin-bottom: 25px;">
                                    <div class="signle-date-time-wed-party">
                                        <h3>Akad Nikah</h3>
                                        <h4>16.00 s/d 17.00 WIB</h4>
                                        <br><a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=N2c4bTZ1Z2sxcjNyOThwZ2NtamxhcWtvaWQgeXRzYXR1dTAyQG0&amp;tmsrc=ytsatuu02%40gmail.com"><button type="button">Simpan Di Kalender</button></a>
                                    </div>
                                </div> <!-- ./End Single Date and Time Wedding Party -->

 <!--                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin-bottom: 25px;">
                                    <div class="signle-date-time-wed-party">
                                        <h3>Resepsi</h3>
                                        <h4>18.00 s/d 20.00 WIB</h4>
                                        <br><a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=NGZuN2k1dWU0MDBkMDZya2hqdG12NjhidWwgeXRzYXR1dTAyQG0&amp;tmsrc=ytsatuu02%40gmail.com"><button type="button">Simpan Di Kalender</button></a>
                                    </div>
                                </div> <!-- ./End Single Date and Time Wedding Party -->

 <!--                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin-bottom: 25px;">
                                    <div class="signle-date-time-wed-party">
                                        <h3>Lokasi</h3>
                                        <h4>Gedung Grand Admiral, Semarang <br> Jl. Ki Mangunsarkoro No.38 Karangkidul, Semarang Tengah</h4>
                                    </div>
                                </div> <!-- ./End Single Date and Time Wedding Party -->

                            </div>
                        </div> <!-- ./End Date And Time Wedding Party -->

                        <div class="map-wedding-party">
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.1417576430013!2d110.42696421450968!3d-6.992579770424408!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708ca102237727%3A0xd7c9d352cc279996!2sGrand%20Admiral%20Ballroom!5e0!3m2!1sid!2sid!4v1624185529035!5m2!1sid!2sid" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy">
                          </iframe>

                            <!--<div id="map-canvas"></div>
                                <div class="map-address-wed-party">
                                  <h6>Restaurant "The Lasiss"</h6>
                                  <p>Anrold Street No.73 587651, lorem California</p>
                            </div>-->
                        </div> <!-- ./End Map Wedding Party -->


                    </div> <!-- ./End Wedding Party Column -->
                    -->


                </div> <!-- ./End Inner Wedding Party -->
            </div>
        </section><!-- ./End Wedding Party -->
