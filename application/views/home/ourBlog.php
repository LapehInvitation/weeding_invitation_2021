
        <!-- Our Blog -->
        <section id="pixiefy-wedz-our-blog" class="wedz-section">
            <div class="container">
                <div class="inner-wedz-our-blog">

                    <div class="wedz-section-header">
                        <img src="<?= base_url() ?>assets/images/wedding-ceremony-secon-icon.png" alt="Section Header Icon" class="img-responsive">
                        <h2>Our Blog</h2>
                    </div> <!-- End Wedz Section Header -->

                    <div class="wedz-blog-main-content">
                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="single-blog-content">
                                    <figure>
                                        <div class="our-blog-thumb">
                                            <a class="partystock-img" href="blog-single.html"><img src="<?= base_url() ?>assets/images/blog-thumb.jpg" alt="Our Blog Thumb" class="img-responsive"><div class="single-wedz-photo-overley"></div></a>
                                        </div>

                                        <figcaption>
                                            <header>
                                                <a href="blog.html"><h4>Our Story</h4></a>
                                                <small>25 October 2015</small>
                                            </header>

                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div> <!-- ./End Single Blog Content -->

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="single-blog-content">
                                    <figure>
                                        <div class="our-blog-thumb">
                                            <a class="partystock-img" href="blog-single.html"><img src="<?= base_url() ?>assets/images/bachelor_party.jpg" alt="Our Blog Thumb" class="img-responsive"><div class="single-wedz-photo-overley"></div></a>
                                        </div>

                                        <figcaption>
                                            <header>
                                                <a href="blog.html"><h4>Bachelor's Party</h4></a>
                                                <small>25 October 2015</small>
                                            </header>

                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div> <!-- ./End Single Blog Content -->

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="single-blog-content">
                                    <figure>
                                        <div class="our-blog-thumb">
                                            <a class="partystock-img" href="blog-single.html"><img src="<?= base_url() ?>assets/images/bridal_party.jpg" alt="Our Blog Thumb" class="img-responsive"><div class="single-wedz-photo-overley"></div></a>
                                        </div>

                                        <figcaption>
                                            <header>
                                                <a href="blog.html"><h4>Bridal Party</h4></a>
                                                <small>25 October 2015</small>
                                            </header>

                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div> <!-- ./End Single Blog Content -->

                        </div>
                        <div class="wedz-blog-more">
                            <a href="blog.html" class="wedz-button">Read More</a>
                        </div>
                    </div> <!-- ./End Blog Main Content -->

                </div> <!-- ./End Inner Wedz Our Blog -->
            </div>
        </section><!-- ./End Our Blog -->