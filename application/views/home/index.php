<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from pixiefy.com/themes/wedz/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 01 May 2021 06:03:48 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Ridlo & Lusi</title>
    <meta name="description" content="Bootstrap 3 Wedding Template by pixiefy">
    <meta name="author" content="pixiefy">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?= base_url() ?>assets/images/favicon/favicon.ico">
    <link rel="apple-touch-icon" href="<?= base_url() ?>assets/images/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>assets/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>assets/images/favicon/apple-touch-icon-114x114.png">

    <link href='https://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:100,400,300,700,800' rel='stylesheet' type='text/css'>


    <!-- All CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/nivo-lightbox.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/themes/default/default.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/component.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/superslides.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.theme.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.transitions.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/mukadimah.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/sideMenu.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/galery.css">

    <script src="<?= base_url() ?>assets/js/modernizr-2.8.3.min.js"></script> <!-- required for gallery -->

    <!-- Condition script for ie for responsive and html5 -->
    <!--[if lt IE 9]>
            <script src="<?= base_url() ?>assets/https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="<?= base_url() ?>assets/https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    <style type="text/css">
        @media only screen and (max-width: 360px) {
            .single-header-content {
                width: 100% !important;
            }
        }
    </style>
    <!-- Our Story -->
    <style>
        #overlay {
            position: fixed;
            display: block;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: red;
            z-index: 121212122;
            cursor: pointer;
        }
    </style>

    <style type="text/css">
        @font-face {
            font-family: 'Dancing Script';
            src: url('<?= base_url(); ?>assets/fonts/dancing_script.ttf');
        }

        @font-face {
            font-family: 'Damion';
            src: url('<?= base_url(); ?>assets/fonts/damion_regular.ttf');
        }

        @font-face {
            font-family: 'Aladin';
            src: url('<?= base_url(); ?>assets/fonts/aladin_regular.ttf');
        }

        @font-face {
            font-family: 'Rancho';
            src: url('<?= base_url(); ?>assets/fonts/rancho_regular.ttf');
        }

        .tombol {
            background: none !important;
            color: #fff !important;
            border: solid 2px #ccc !important;
        }

        .tombol:hover {
            background-color: #000 !important;
            opacity: 0.4;
        }

        .p2 {
            font-family: "Aladin" !important;
            /* font-size:35px !important; */
        }

        .p3 {
            font-family: "Rancho" !important;
            font-size: 15px !important;
        }

        .musicIcon {
            left: 15px;
            position: fixed;
            background-color: transparent;
            top: 75px;
            z-index: 999;
        }

        .loading-love {
            z-index: 99912;
        }

        .fa-volume-up,
        .fa-volume-off {
            text-shadow: -1px 0 #fff, 0 1px #fff, 1px 0 #fff, 0 -1px #fff;
        }

        .fa-volume-up:hover,
        .fa-volume-up:focus,
        .fa-volume-up:active,
        .fa-volume-up:visited {
            color: #cabaac !important;
        }

        .fa-volume-off:hover,
        .fa-volume-off:focus,
        .fa-volume-off:active,
        .fa-volume-off:visited {
            color: #cabaac !important;
        }

        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 99999234991;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 90%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        /* animation */
        .fadein {
            opacity: 0;
        }


        @media only screen and (max-width: 360px) {
            .single-header-content p {
                font-size: none !important;
            }

            .single-header-content {
                width: 100% !important;
            }

            .p3 {
                font-size: 17px !important;
            }
        }
    </style>
</head>

<!-- MODAL COVID -->
<div id="covid_modal" class="modal">
    <div class="modal-content">
        <span class="close_covid" style="float: right; ">
            <i class="fa fa-times " aria-hidden="true"></i>
        </span>
        <img class="img-responsive" src="<?= base_url() ?>assets/images/prokes.jpg" alt="">
    </div>
</div>
<!-- END COVID -->

<body id="home" class="home">
    <!-- please add home class on home page -->
    <div id="wedz-preloader loading-love"></div>
    <div id="overlay">
        <div class="container">
            <div class="inner-wedz-header-section">

                <div class="wedz-header-main-content">
                    <div class="row">

                        <div class="single-header-content">
                            <img src="<?= base_url() ?>assets/images/wedz-rose.png" alt="Wedz Rose" class="img-responsive wow fadeInDown" data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">
                            <br>
                            <h1 class="wow fadeInUp" data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">
                                Ridlo<br>&<br>Lusi
                            </h1>
                            <p class="wow fadeInUp" data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">
                                <span class="p3">Kepada Bapak/Ibu/Saudara/i</span><br>
                                <span class="p2"><b> Sekeluarga</b></span><br>
                                <span class="p3">Mohon maaf bila ada kesalahan pada penulisan nama/gelar</span>
                            </p>
                            <button onclick="off()" class="btn btn-default btn-bordered tombol" style="margin-bottom: 3%;font-size:20px">
                                Buka Undangan
                            </button>
                        </div>

                    </div>
                </div> <!-- ./End Wedz Header Main Content -->

            </div>
        </div>
    </div>

    <div class="wedz-hero">
        <div id="wedz-hero-slides">
            <div class="slides-container">
                <img src="<?= base_url() ?>assets/images/slide1.jpg" style="width:100%" alt="Wedz Hero Slider">
                <img src="<?= base_url() ?>assets/images/slide2.jpg" style="width:100%" alt="Wedz Hero Slider">
                <img src="<?= base_url() ?>assets/images/slide3.jpg" style="width:100%" alt="Wedz Hero Slider">
                <img src="<?= base_url() ?>assets/images/slide4.jpg" style="width:100%" alt="Wedz Hero Slider">
            </div>
        </div>
        <!-- Header -->
        <header id="pixiefy-wedz-header-section">
            <div class="container">
                <div class="inner-wedz-header-section">

                    <!-- <div class="wedz-logo">
                            <a href="#"><img src="<?= base_url() ?>assets/images/logo.png" alt="Wedz Logo" class="img-responsive"></a>
                        </div>  -->

                    <div class="wedz-header-main-content">
                        <div class="row">

                            <div class="single-header-content">
                                <img src="<?= base_url() ?>assets/images/wedz-rose.png" alt="Wedz Rose" class="img-responsive wow fadeInDown" data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">
                                <br>
                                <h1 class="wow fadeInUp" data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">Ridlo & Lusi</h1>
                                <p class="wow fadeInUp" data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">Are getting married</p>
                            </div>

                        </div>
                    </div> <!-- ./End Wedz Header Main Content -->

                </div>
            </div>
        </header>
        <a id="hero_over_link" href="#pixiefy-wedz-our-story"></a>
    </div> <!-- ./End HERO -->
    <script>
        var modal = document.getElementById("covid_modal");
        var span = document.getElementsByClassName("close_covid")[0];
        var audio = new Audio("<?= base_url() ?>assets/lagu2.mp3");
        audio.loop = true;

        function off() {
            document.getElementById("overlay").style.display = "none";
            document.getElementById("home").style.overflow = "hidden";
            modal.style.display = "block";
            audio.play();
        }

        function play() {
            document.getElementById("playBtn").style.display = 'block';
            document.getElementById("pauseBtn").style.display = 'none';
            audio.play();
        }

        function pause() {
            document.getElementById("pauseBtn").style.display = 'block';
            document.getElementById("playBtn").style.display = 'none';
            audio.pause();
        }

        span.onclick = function() {
            modal.style.display = "none";
            document.getElementById("home").style.overflow = "visible";
        }


        setTimeout(function() {
            modal.style.display = "none";
            document.getElementById("home").style.overflow = "visible";
        }, 15000);
    </script>


    <div class="musicIcon">

        <ul>
            <li>
                <a><i class="fa fa-volume-up fa-3x" aria-hidden="true" id="playBtn" onclick="pause()"></i></a>
            </li>
            <li>
                <a><i class="fa fa-volume-off fa-3x" style="display:none" aria-hidden="true" id="pauseBtn" onclick="play()"></i></a>
            </li>
        </ul>
    </div>