<!-- Our Love Life -->
<style>
    .top5{
        margin-top:5%;
    }
</style>
<section id="pixiefy-wedz-love-life" class="section-padding wedz-section">
    <div class="container">
        <div class="inner-love-life">

            <div class="wedz-section-header fadein">
                <img src="<?= base_url() ?>assets/images/couple-parents-icon.png" alt="Section Header Icon" class="img-responsive">
                <h2>Perjalanan Cinta Kami</h2>
            </div> <!-- End Wedz Section Header -->

            <div class="row">
                <div class="wedz-our-love-life-promo">

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fadein top5">
                        <div class="single-love-life-promo">
                            <h3>Awal Perkenalan Kami</h3>
                            <p> Jawa Tengah, Januari 2021. Hal tak terduga ketika keluarga memperkenalkan kami, seperti melihat malaikat yang hadir dengan sinarnya. Inikah yang akan menyinari hidupku?  </p>
                        </div>
                    </div><!--  ./End Single Love Life Promo -->
                    <br>
                    <br>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fadein top5">
                        <div class="single-love-life-promo">
                            <h3>Awal Pertemuan Kami</h3>
                            <p> Magelang, 14 Februari 2021. Pertemuan tak'kan pernah kami lupakan. Harapkan terucap lebih dari sebuah ekspektasi, seperti sebuah sepasang kunci dan gembok, langsung klik.  </p>
                        </div>
                    </div><!--  ./End Single Love Life Promo -->
                    <br>
                    <br>
                    <br>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fadein top5">
                        <div class="single-love-life-promo">
                            <h3>Pertunangan Kami</h3>
                            <p> Magelang, 10 April 2021. Hari yang kami tunggu telah tiba, dihadapan keluarga besar dan sahabat kami mengikrarkan diri berkomitmen menjadi sebuah pasangan abadi </p>
                        </div>
                    </div><!--  ./End Single Love Life Promo -->

                </div>
            </div> <!-- ./End Our Love Life Promo -->

        </div> <!-- ./End Inner Love Life -->
    </div>
</section><!-- ./End Our Love Life -->