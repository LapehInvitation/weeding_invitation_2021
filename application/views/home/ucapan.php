<!-- Reservation Section -->
<style>
    .hasilSearch{
        background-color:white;
        color:black;
        overflow:scroll;
        height:350px;
        padding-left:5%;
        padding-top:5%;
    }
    ul{
        padding:0 2% !important;
    }
    .chat{
        width:90%;
        flex: 1;
        display: block;
        background-color: #917E6D;
        border-radius: 8px;
        box-shadow: 0 1px 1px rgba(0, 0, 0, .15);
        margin-left: 15px;
        min-height: 60px;
        position: relative;
        padding: 8px;
        position: relative;
        margin-bottom: 10px;
        word-wrap:break-word;
    }
    .overlay-ucapan{
        background-color:#222227 !important;
    }

    .buttonPesan{
        font-size: 18px !important; 
        padding: 10px 55px;
        border-radius: 9px;
    }
</style>
<section id="pixiefy-wedz-reservation-section" class="section-padding wedz-section">
    <div class="container">
        <div class="inner-reservation-section">

            <div class="wedz-section-header">
                <img src="<?= base_url() ?>assets/images/wedding-ceremony-secon-icon.png" alt="Section Header Icon" class="img-responsive">
                <h2>Ucapan & Doa</h2>
            </div> <!-- End Wedz Section Header -->


            <div class="wedz-main-reservation-content">
                <div class="right-form-reservation">

                    <?php if($sesion_data['kode']!="kosong"): ?>
                        <div class="single-left-rsvp">
                            <div class="single-form-item">
                                <label for="wedz_name">Nama *</label>
                                <input placeholder="Tulis Nama" name="nama" id="search_inponow" type="text" value="<?= $sesion_data['nama'] ?>" disabled readonly required>
                            </div> 
                        </div>
                        <div class="single-right-rsvp">
                            <div class="single-form-item">
                                <label for="wedz_message">Tulis Pesan *</label>
                                <textarea placeholder="Sampaikan Pesan dan Ucapan" id="wedz_message" name="ucapan" style="height:125px" maxlength="160" onKeyUp='HitungText()' required></textarea>
                                <br>
                                <span align='center' id='hasil'>0/160</span>
                            </div>
                        </div>
                        <div class="single-form-item full-width-item">
                                <button type="submit" id="simpan_ucapan" class="buttonPesan wedz-button" onclick="SearchAjax()">Kirim Ucapan</button>
                        </div>
                    <?php endif;?>
                        <div class="row">
                            <div  id="hasilSearch" class="col-md-12 hasilSearch">

                                <?php foreach($ucapan as $up): ?>
                                    <ul class="chat">
                                        <b><?= $up['nama'] ?></b><br>
                                        <?= $up['ucapan'] ?>
                                    </ul>
                                    <br>
                                <?php endforeach;?>
                            </div>
                        </div>
                </div> <!-- ./End right Form Reservation -->
            </div><!--  ./End Main Reservation Content -->

        </div> <!-- ./End Inner Reservation Section -->
    </div>
    <div class="wedz-section-overley overlay-ucapan"></div>
</section><!-- ./End Reservation Section -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.js" ></script>
<script type="text/javascript">
  function SearchAjax() {
    var isi = $('#search_inponow').val();
    var isi2 = $('#wedz_message').val();
    if (isi2 != "") {
        document.getElementById('wedz_message').value = '';
        $("#hasilSearch").empty();
        console.log(isi);
        $.ajax({
        method: 'POST',
        dataType: 'json',
        url: '<?= base_url() ?>ridlolusi/getDataFromAjx',
        data: {
            input_ajx: isi,
            input_ajx2: isi2
        },
        success: function(result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                var isivalue = "<ul class='chat'><b>" + result[i].nama + "</b><br>" + result[i].ucapan + "</ul><br>";
                $(".hasilSearch").append(isivalue);
            }
        }
        })
    }
    // document.getElementById('search_inponow').value = '';
  }
</script>

<script language='javascript'>
    function HitungText(){
        var Teks = document.getElementById('wedz_message').value.length;
        var total = document.getElementById('hasil');
        total.innerHTML = Teks + '/160';
    }
</script>