<!doctype html>
<html class="no-js" lang="">

<!-- Mirrored from pixiefy.com/themes/wedz/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 01 May 2021 06:03:48 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Ridlo & Lusi</title>
        <meta name="description" content="Bootstrap 3 Wedding Template by pixiefy">
        <meta name="author" content="pixiefy">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="<?= base_url() ?>assets/images/favicon/favicon.ico">
        <link rel="apple-touch-icon" href="<?= base_url() ?>assets/images/favicon/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>assets/images/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>assets/images/favicon/apple-touch-icon-114x114.png">

        <link href='https://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:100,400,300,700,800' rel='stylesheet' type='text/css'>


        <!-- All CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/nivo-lightbox.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/themes/default/default.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/component.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/superslides.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.theme.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.transitions.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/style.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css">
        <script src="<?= base_url() ?>assets/js/modernizr-2.8.3.min.js"></script> <!-- required for gallery -->

        <!-- Condition script for ie for responsive and html5 -->
        <!--[if lt IE 9]>
            <script src="<?= base_url() ?>assets/https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="<?= base_url() ?>assets/https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style type="text/css">
            @font-face {
                font-family: 'Dancing Script';
                src: url('<?= base_url(); ?>assets/fonts/dancing_script.ttf');
            }
            @font-face {
                font-family: 'Damion';
                src: url('<?= base_url(); ?>assets/fonts/damion_regular.ttf');
            }
            @font-face {
                font-family: 'Aladin';
                src: url('<?= base_url(); ?>assets/fonts/aladin_regular.ttf');
            }
            @font-face {
                font-family: 'Rancho';
                src: url('<?= base_url(); ?>assets/fonts/rancho_regular.ttf');
            }
            .tombol{
                background:none !important;
                color:#fff !important;
                border:solid 2px #ccc !important;
            }
            .tombol:hover{
                background-color:#000 !important;
                opacity:0.4;
            }
            .p2{
                font-family:"Aladin" !important;
                /* font-size:35px !important; */
            }
            .p3{
                font-family:"Rancho" !important;
                font-size:15px !important;
            }
            @media only screen and (max-width: 360px) {
                .single-header-content p{
                    font-size:none !important;
                }
                .single-header-content{
                    width:100% !important;
                }
                .p3{
                    font-size:17px !important;
                }
            }
        </style>

    </head>

    <body class="home"> <!-- please add home class on home page -->

        <div id="wedz-preloader"></div>

        <div class="wedz-hero">
            <div id="wedz-hero-slides">
                <div class="slides-container">
                    <img src="<?= base_url() ?>assets/images/slide1.jpg" style="width:100%" alt="Wedz Hero Slider">
                    <img src="<?= base_url() ?>assets/images/slide2.jpg" style="width:100%" alt="Wedz Hero Slider">
                    <img src="<?= base_url() ?>assets/images/slide3.jpg" style="width:100%" alt="Wedz Hero Slider">
                    <img src="<?= base_url() ?>assets/images/slide4.jpg" style="width:100%" alt="Wedz Hero Slider">
                </div>
            </div>
            <!-- Header -->
            <header id="pixiefy-wedz-header-section">
                <div class="container">
                    <div class="inner-wedz-header-section">

                        <!-- <div class="wedz-logo">
                            <a href="#"><img src="<?= base_url() ?>assets/images/logo.png" alt="Wedz Logo" class="img-responsive"></a>
                        </div>  -->

                        <div class="wedz-header-main-content">
                            <div class="row">

                            <div class="single-header-content">
                                    <img src="<?= base_url() ?>assets/images/wedz-rose.png" alt="Wedz Rose" class="img-responsive wow fadeInDown"  data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">
                                    <br>
                                    <h1 class="wow fadeInUp" data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">
                                        Ridlo<br>&<br>Lusi
                                    </h1>
                                    <p class="wow fadeInUp" data-wow-delay="1s" data-wow-offset="0" data-wow-duration="1.5s">
                                        <span class="p3">Kepada Bapak/Ibu/Saudara/i</span><br>
                                        <span class="p2"><b><?=$undangan ?> Sekeluarga</b></span><br>
                                        <span class="p3">Mohon maaf bila ada kesalahan pada penulisan nama/gelar</span>
                                    </p>
                                    <a href="<?= base_url(); ?>">
                                        <button class="btn btn-default btn-bordered tombol" style="margin-bottom: 3%;font-size:20px">
                                            Buka Undangan
                                        </button>
                                    </a>
                                </div>

                            </div>
                        </div> <!-- ./End Wedz Header Main Content -->

                    </div>
                </div>
            </header>
        </div> <!-- ./End HERO -->



        <!-- WEDZ JS FILES -->
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.0.2.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/nivo-lightbox.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/classie.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/photostack.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;AMP;"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/wow.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.superslides.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/smoothscroll.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/main.js"></script>

    </body>

<!-- Mirrored from pixiefy.com/themes/wedz/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 01 May 2021 06:04:27 GMT -->
</html>
