<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model
{

    public function __construct(){
        parent::__construct();
        //load database dtks
    }

    public function get_ucapan(){
        $this->db->order_by('id','desc');
        $this->db->from('ucapan');
        $query = $this->db->get();
        $ucapan =  $query->result_array();
        
        return $ucapan;
    }

    public function getdataAjaxInponow($nama,$ucapan) { 
        $data = [
            "nama" => $nama,
            "ucapan" => $ucapan
        ];
        $this->db->insert('ucapan',$data);

        $this->db->order_by('id','desc');
        $this->db->from('ucapan');
        $query = $this->db->get();
        $ucapan =  $query->result_array();
        
        return $ucapan; 
    }
		
    public function logged_id(){
        return $this->session->userdata('kode');
    }

    public function get_logged_id($id){
        return $this->db->get_where('tamu', ['kode'=>$id])->row_array();
    }











}
?>