<?php

	class Ridlolusi extends CI_Controller{
		public function __construct(){
            parent::__construct();
            $this->load->model('Admin_model');
			$this->load->helper(array('form','url'));
        }

		public function index(){
			$data['sesion_data'] = $this->Admin_model->logged_id();
			$the_id = $data['sesion_data'];
			if($the_id==null){
				$data['sesion_data'] = array("kode"=>"kosong","nama"=>"Anonyomus");
			}
			else{
				$data['sesion_data'] = $this->Admin_model->get_logged_id($the_id);
			}
			
			
			$data['ucapan'] = $this->Admin_model->get_ucapan();
            $this->load->view('home/index');
			$this->load->view('home/menuBar');
            $this->load->view('home/mukadimah');
            $this->load->view('home/ceremony');
            $this->load->view('home/perjalananCinta');
            $this->load->view('home/galeryFoto');
            $this->load->view('home/ucapan',$data);
            $this->load->view('home/footer');
		}
	
		public function undangan($undangan){
			$this->db->where('kode',$undangan);
			$query = $this->db->get('tamu');
			if($query->row_array()==null){
				$data['undangan']= "Anonyomus";
				$session_data = array(
					'kode' => "kosong",
					// 'nama'   => "Anonyomus"
				);
				//set session userdata
				$this->session->set_userdata($session_data);
				$this->load->view('home/undangan',$data);
			}
			else{
				$data['tamu'] = $query->row_array();
				$data['undangan']= $data['tamu']['nama'];
				$session_data = array(
					'kode' => $data['tamu']['kode'],
					'nama' => $data['undangan']
				);
				//set session userdata
				$this->session->set_userdata($session_data);
				$this->load->view('home/undangan',$data);	
			}
		}
	
		public function ucapan(){

			$data = $this->input->post('nama_input'); 
			// bisa juga menggunakaan-> $_POST['input_ajx'] 
			$ini = $this->Admin_model->getdataAjaxInponow($data); 
			echo json_encode($ini); 

		}
		
		public function getDataFromAjx() { 
			$data = $this->input->post('input_ajx'); 
			$data2 = $this->input->post('input_ajx2'); 
			$ini = $this->Admin_model->getdataAjaxInponow($data,$data2); 
			echo json_encode($ini); 
		  }

		public function load_ucapan(){
			$nama = $this->input->post('nama_input'); 
			$ucapan = $this->input->post('ucapan_input'); 
			// bisa juga menggunakaan-> $_POST['input_ajx'] 
			$ini = $this->Admin_model->getdataAjaxInponow($nama,$ucapan); 
			echo json_encode($ini); 
		}

		public function generate(){
			$huruf = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
			$huruf2 = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
			$id="";
			for($x=0;$x<150;$x++){
				$a = rand(0,25);$b = rand(0,25);$c = rand(0,9);$d = rand(0,9);$e = rand(0,25);$f = rand(0,25);
				$id = $huruf[$a]."".$c."".$huruf2[$b]."".$huruf2[$e]."".$huruf[$f]."".$d;
				$data=[
                    "kode" => $id
                ];
                $this->db->insert('tamu',$data);
			}
		}

		public function logout(){
			$this->session->sess_destroy();
			redirect('ridlolusi');
		}
	
	
	
	
	
	
	
	
	}


?>
